import {useSelector} from 'react-redux';
import {Image, StyleSheet, Text, View} from 'react-native';

export default function Banner() {
  const pseudo = useSelector(state => state.userReducers.pseudo);

  return (
    <View>
      {pseudo !== '' && (
        <View style={styles.banner}>
          <Image
            source={require('../../assets/cravate.jpg')}
            style={{height: 400, position: 'absolute'}}
          />
          <View style={styles.centered}>
            <View style={styles.maaraa}>
              <Text style={styles.maaraaInner}>MAARAA</Text>
            </View>
          </View>
          <Image
            source={require('../../assets/logo.png')}
            style={{width: 75, height: 75, position: 'absolute'}}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  banner: {
    height: 350,
  },
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  maaraa: {
    borderWidth: 1,
    borderColor: 'black',
    borderStyle: 'solid',
    backgroundColor: '#004AAD80',
    height: 150,
    paddingLeft: 20,
    paddingRight: 20,
  },
  maaraaInner: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
    fontSize: 20,
  },
});
