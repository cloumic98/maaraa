import {ScrollView, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';
import Accueil from './screen/Accueil';
import Homme from './screen/Homme';
import Femme from './screen/Femme';
import Promotion from './screen/Promotion';
import {Provider} from 'react-redux';
import {Store} from './redux/store';
import Login from './screen/Login';
import Banner from './utils/Banner';

const Tab = createMaterialTopTabNavigator();

const App = () => {
  return (
    <Provider store={Store}>
      <View style={{flex: 1}}>
        {/* MAARAA BANER */}
        <Banner />
        {/* NAVIGATOR */}
        <NavigationContainer>
          <Tab.Navigator
            initialRouteName={'login'}
            screenOptions={{
              tabBarActiveTintColor: 'white',
              tabBarInactiveTintColor: 'gray',
              tabBarIndicatorStyle: {
                backgroundColor: '#004AAD',
              },
              tabBarStyle: {
                backgroundColor: '#004AAD',
              },
              tabBarLabelStyle: {
                textTransform: 'none',
                fontSize: 11,
              },
            }}>
            <Tab.Screen
              name={'login'}
              component={Login}
              options={{tabBarStyle: {display: 'none'}, swipeEnabled: false}}
            />
            <Tab.Screen
              name={'nouveaute'}
              component={Accueil}
              options={{title: 'Nouveautés'}}
            />
            <Tab.Screen
              name={'homme'}
              component={Homme}
              options={{title: 'Homme'}}
            />
            <Tab.Screen
              name={'femme'}
              component={Femme}
              options={{title: 'Femme'}}
            />
            <Tab.Screen
              name={'promotion'}
              component={Promotion}
              options={{title: 'Promotions'}}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </View>
    </Provider>
  );
};

export default App;
