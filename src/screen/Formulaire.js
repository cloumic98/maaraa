import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Button,
  Modal,
  Pressable,
} from 'react-native';
import Picker from '@ouroboros/react-native-picker';
import {useState} from 'react';
import {RadioButton} from 'react-native-paper';

const Formulaire = () => {
  let [pickerPays, setPickerPays] = useState('FR');
  let [pickerRegion, setPickerRegion] = useState(null);
  let [checked, setChecked] = useState('first');
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View>
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View>
          <View style={styles.centeredView}>
            <Text style={styles.modalText}>
              Votre message a bien été envoyer a notre équipe
            </Text>
            <Pressable onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.textStyle}>FERMER</Text>
            </Pressable>
          </View>
        </View>
      </Modal>

      <Text style={styles.sendMessage}>ENVOYER UN MESSAGE</Text>
      <View style={styles.body}>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Nom</Text>
          </View>
          <TextInput style={styles.input} placeholder="ex: Duranton" />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Prénom</Text>
          </View>
          <TextInput style={styles.input} placeholder="ex: Alfred" />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Homme</Text>
          </View>
          <RadioButton
            value="first"
            status={checked === 'first' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('first')}
          />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Femme</Text>
          </View>
          <RadioButton
            value="second"
            status={checked === 'second' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('second')}
          />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Autre</Text>
          </View>
          <RadioButton
            value="third"
            status={checked === 'third' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('third')}
          />
        </View>

        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Votre adresse</Text>
          </View>
          <TextInput style={styles.input} placeholder="ex: 12 rue de la rue" />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Votre ville de résidence</Text>
          </View>
          <TextInput style={styles.input} placeholder="ex: Paris" />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Votre code Postal</Text>
          </View>
          <TextInput
            style={styles.input}
            placeholder="ex: 65423"
            keyboardType="numeric"
          />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Région</Text>
          </View>
          {pickerPays == 'FR' ? (
            <Picker
              style={{
                borderWidth: 1,
                borderColor: '#a7a7a7',
                borderRadius: 5,
                marginBottom: 5,
                padding: 5,
              }}
              onChanged={setPickerRegion}
              options={[
                {value: 'NOR', text: 'NORMANDIE'},
                {value: 'ILE', text: 'ILE-DE-FRANCE'},
                {value: 'ALS', text: 'ALSACE'},
              ]}
              value={pickerRegion}
            />
          ) : (
            <Picker
              style={{
                borderWidth: 1,
                borderColor: '#a7a7a7',
                borderRadius: 5,
                marginBottom: 5,
                padding: 5,
              }}
              onChanged={setPickerRegion}
              options={[
                {value: 'CAL', text: 'CALIFORNIA'},
                {value: 'MON', text: 'MONTANA'},
                {value: 'TEX', text: 'TEXAS'},
              ]}
              value={pickerRegion}
            />
          )}
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Pays</Text>
          </View>
          <Picker
            style={{
              borderWidth: 1,
              borderColor: '#a7a7a7',
              borderRadius: 5,
              marginBottom: 5,
              padding: 5,
            }}
            onChanged={setPickerPays}
            options={[
              {value: 'FR', text: 'France'},
              {value: 'US', text: 'United-State'},
            ]}
            value={pickerPays}
          />
        </View>
        <View style={styles.oneLine}>
          <View style={{width: 150}}>
            <Text style={{textAlign: 'center'}}>Votre message</Text>
          </View>
          <TextInput style={styles.input} multiline={true} numberOfLines={4} />
        </View>
        <View style={styles.button}>
          <Button
            style={styles.buttonWidth}
            onPress={() => setModalVisible(true)}
            title="Envoyer"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: '#967EEC',
  },
  sendMessage: {
    textAlign: 'center',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 10,
    color: '#991E84',
  },
  input: {
    height: 40,
    width: 200,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  oneLine: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonWidth: {
    width: 200,
  },
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    backgroundColor: 'gray',
  },
});

export default Formulaire;
