import { ScrollView, StyleSheet, Text, View } from "react-native";
import Formulaire from './Formulaire';

export default function Accueil() {
  return (
    <ScrollView style={styles.background}>
      <View style={styles.titleBackground}>
        <Text style={{ color: 'blue', fontWeight: 'bold', fontStyle: 'italic' }}>Bienvenue chez MAARAA :Le site de référence en matière de luxe!</Text>
      </View>
      <View style={styles.description}>
        <Text style={{ color: 'black', fontSize: 18 }}>Maaraa est une destination de choix dans le commerce en ligne et prêt-à-porter, sacs, chaussures et accessoires de luxe mettant à votre disposition une game regroupant plus de 200 des créateurs les plus convoités tels de Prada, Givenchy et Burberry. Nous sommes fiers de vous proposer une plateforme de shopping en ligne qui met un point d'honneur à conserver une expérience digne d'une boutique.</Text>
      </View>
      <Formulaire />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: 'lightgray',
  },
  titleBackground: {
    backgroundColor: 'gray',
    margin: 10,
  },
  description: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'black',
    margin: 10,
  },
})
