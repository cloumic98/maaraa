import React, {useState} from 'react';
import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {setLogin, setLogout} from '../redux/action';
import {Store} from '../redux/store';

export const Login = ({navigation}) => {
  const [pseudo, setPseudo] = useState('');
  const [password, setpassword] = useState('');
  const [connected, setConnected] = useState(
    Store.getState().userReducers.pseudo != '',
  );
  const dispatch = useDispatch();

  return (
    <View style={styles.back}>
      <View style={styles.imageView}>
        <Image
          style={{width: 150, height: 150}}
          source={require('../../assets/logo.png')}
        />
      </View>
      {!connected && (
        <View style={styles.inputView}>
          <TextInput
            placeholder="Email or Username"
            style={styles.input}
            onChangeText={e => {
              setPseudo(e);
            }}
          />
          <TextInput
            placeholder="Password"
            style={styles.input}
            secureTextEntry={true}
            onChangeText={e => {
              setpassword(e);
            }}
          />
        </View>
      )}
      {connected && (
        <View>
          <Text>Bonjour {Store.getState().userReducers.pseudo}</Text>
        </View>
      )}

      <Pressable
        style={styles.button}
        onPress={() => {
          if (connected) {
            dispatch(setLogout());
            setConnected(false);
          } else {
            dispatch(setLogin({pseudo: pseudo, password: password}));
            setConnected(true);
            navigation.navigate('nouveaute');
          }
        }}>
        <Text style={styles.buttonText}>
          {connected ? 'Log out' : 'Log in'}
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  back: {
    flex: 1,
    backgroundColor: '#207dc3',
    display: 'flex',
    alignItems: 'center',
  },
  input: {
    margin: 5,
    backgroundColor: 'white',
    borderRadius: 6,
    borderStyle: 'solid',
    borderColor: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    width: '90%',
    color: 'black',
  },
  button: {
    backgroundColor: 'rgba(255,255,255, 0.4)',
    height: 50,
    width: '70%',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 4,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  inputView: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  imageView: {
    height: '40%',
    display: 'flex',
    justifyContent: 'center',
  },
});

export default Login;
