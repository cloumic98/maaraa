import {ScrollView, Text, View, StyleSheet, Image} from 'react-native';

export default function Homme() {
  const catalogue = [
    {
      name: 'Hey girl, You are the CSS to my HTML',
      price: 20,
      url: require('../../assets/images/hoodies/Hey-girl-You-are-the-CSS-to-my-HTML-black-hoodie-350x525.jpg'),
    },
    {
      name: "The SV style Men's PrintedHoodie (red hoodie)",
      price: 10,
      url: require('../../assets/images/hoodies/71TmZgVFbFL._UL1500_.jpg'),
    },
    {
      name: 'Cotton Hoodie Printing (black hoodie)',
      price: 10,
      url: require('../../assets/images/hoodies/hoodie-printing-with-hoodies-500x500.jpg'),
    },
  ];

  return (
    <ScrollView>
      {catalogue.map((oneCatalogue, i) => {
        return (
          <View key={i} style={style.catalogueView}>
            <View style={style.imageView}>
              <Image
                source={oneCatalogue.url}
                style={{width: 150, height: 150}}
              />
            </View>
            <Text style={style.nameStyle}>{oneCatalogue.name}</Text>
            <Text style={style.priceStyle}>{oneCatalogue.price}$</Text>
          </View>
        );
      })}
    </ScrollView>
  );
}

const style = StyleSheet.create({
  catalogueView: {
    marginTop: 10,
    marginBottom: 10,
  },
  priceStyle: {
    fontSize: 16,
    color: 'orange',
    textAlign: 'center',
  },
  nameStyle: {
    textAlign: 'center',
    color: 'black',
  },
  imageView: {
    display: 'flex',
    alignItems: 'center',
  },
});
