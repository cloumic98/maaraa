export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const ADD_MERCH = 'ADD_MERCH';
export const DELETE_MERCH = 'DELETE_MERCH';

export const setLogin = credentials => dispacth => {
  dispacth({
    type: LOGIN,
    payload: credentials,
  });
};

export const setLogout = credentials => dispacth => {
  dispacth({
    type: LOGOUT,
  });
};

export const setAddMerch = credentials => dispacth => {
  dispacth({
    type: ADD_MERCH,
    payload: credentials,
  });
};

export const setDeleteMerch = credentials => dispacth => {
  dispacth({
    type: DELETE_MERCH,
    payload: credentials,
  });
};
