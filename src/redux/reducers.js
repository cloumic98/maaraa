import {ADD_MERCH, DELETE_MERCH, LOGIN, LOGOUT} from './action';

const initialState = {
  pseudo: '',
  password: '',
  panier: [],
};

export function userReducers(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        pseudo: action.payload.pseudo,
        password: action.payload.password,
      };
    case ADD_MERCH:
      return {...state, panier: action.payload.panier};
    case DELETE_MERCH:
      return {...state, panier: action.payload.panier};
    case LOGOUT:
      return {...initialState};
    default:
      return state;
  }
}
