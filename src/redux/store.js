import {applyMiddleware, combineReducers, createStore} from 'redux';
import {userReducers} from './reducers';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({userReducers});

export const Store = createStore(rootReducer, applyMiddleware(thunk));
